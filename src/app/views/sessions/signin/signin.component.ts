import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import {Router} from '@angular/router';

import {AccountService,IdleService} from '@domain/adm/index.adm';
import {environment} from '@environments/environment';

@Component({
  selector: 'app-signin2',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

  signupForm: FormGroup;
  errorMsg: string = null;

  constructor(
    private accountService: AccountService,
    private router: Router,
    private fb: FormBuilder) {}

  ngOnInit() {

    //redirect to home
    if(this.accountService.isAuthenticated()){
      this.router.navigate([environment.redirectUrl]);
    }

    const password = new FormControl('', Validators.required);
    const confirmPassword = new FormControl('', CustomValidators.equalTo(password));

    this.signupForm = this.fb.group(
      {
        email: ["",[Validators.required,Validators.email]],
        password: password,
        agreed: [false,Validators.required]
      }
    );

    
  }

  // convenience getter for easy access to form fields
  get f() { return this.signupForm.controls; }

  onSubmit() {
        if (this.signupForm.invalid) {
          // do what you wnat with your data
          console.log(this.signupForm.value);
          return;
        }

        this.accountService.login(this.f.email.value, this.f.password.value).subscribe(result => {
          if (result.success) {
            this.accountService.startIdle();
            this.router.navigate([environment.redirectUrl]);
          } else {
            this.errorMsg = result.msg;
          }
      });
  
  }

}
