import {Injectable, Inject} from '@angular/core';
import {Router} from '@angular/router';
import {Idle, DEFAULT_INTERRUPTSOURCES} from '@ng-idle/core';
import {Keepalive} from '@ng-idle/keepalive';
import { environment } from '@environments/environment';
import { UserModel } from '../models/user.model';



@Injectable({
  providedIn: 'root'
})
export class IdleService {

  user: UserModel;
  redirectUrl = environment.redirectUrl;

  constructor(private idle: Idle, 
            private keepalive: Keepalive, 
            private router: Router) {

    console.log("my idle");
    idle.setIdle(600);

    idle.setTimeout(15);

    idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

    idle.onIdleEnd.subscribe(() => console.log('No longer idle.'));
    idle.onTimeout.subscribe(() => {
      console.log('time out');
      this.clearDatas();
      this.router.navigate([environment.signinUrl]);
    });

    idle.onIdleStart.subscribe(() => console.log('You\'ve gone idle!'));
    idle.onTimeoutWarning.subscribe((countdown) => {
      console.log('You will time out in ' + countdown + ' seconds!')
    });

    // sets the ping interval to 15 seconds
    keepalive.interval(15);

    keepalive.onPing.subscribe(() => {
      console.log('Keep me alive');
       //this.keepAliveService.keepAlive();
    });
  }


  clearDatas() {
    this.user = new UserModel();
    localStorage.removeItem("currentUser");
    console.log('clear', this.user);
    this.idle.stop();
  }
}
