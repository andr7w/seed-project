import {Injectable, Inject} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {Idle, DEFAULT_INTERRUPTSOURCES} from '@ng-idle/core';
import {Keepalive} from '@ng-idle/keepalive';
import {Router} from '@angular/router';
import { environment, urls } from '@environments/environment';
import { UserModel } from '../models/user.model';
import { IdleService } from './idle.service';


@Injectable({
  providedIn : 'root'
})
export class AccountService {
  
  private user: UserModel;
  public currentUser: Observable<UserModel>;
  private currentUserSubject: BehaviorSubject<UserModel>;

  constructor(private http: HttpClient,
              private idleService: IdleService,
              private idle: Idle, 
              private keepalive: Keepalive,
              private router: Router
             ) {

    this.currentUserSubject = new BehaviorSubject<UserModel>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();

    if(this.currentUserValue){
      this.startIdle();
      this.user = this.currentUserValue;
    }else{
      this.user = new UserModel();
    }
                
  }

  login(login: string, passwd: string) {
    
      return this.http.post<any>(`${urls.adm.account.connect}`, { login, passwd })
          .pipe(map(result => {
            if(result.success){
              // store user details and jwt token in local storage to keep user logged in between page refreshes
              this.user = result.user;
              this.user.rights = result.rights;
              localStorage.setItem('currentUser', JSON.stringify(result.user));
              this.currentUserSubject.next(result.user);
            } 
              return result;
          }));
  }

  getUserRights() {
  
    return this.http.get<any>(`${urls.adm.account.rights}`)
        .pipe(map(result => {
          if(result.success){
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            return result.data
          } else{
            return result;
          }
            
        }));
}

  public get currentUserValue(): UserModel {
    return this.currentUserSubject.value;
  }

  public get currentUserRight(): Map<string, boolean> {
      return this.currentUserSubject.value.rights;
  }

  logout() {
      // remove user from local storage to log user out
      localStorage.removeItem('currentUser');
      this.currentUserSubject.next(null);
  }

  keepAlive() {
    console.log("sending keeping alive!!!")
    //return this.accounts.all('keepAlive').customPOST({});
  }

  profile() {
    //return this.accounts.all('keepAlive').customGET();
  }

  startIdle(){
    console.log("starting idle");
    this.idle.watch();
  }


  registerUser(datas, remember: boolean) {
    this.user.login = datas.login;
    this.user.token = datas.login;
    this.user.id = datas.id;
    this.user.isAuth = datas.id > 0;
    

    // this.authData.privileges = datas.privileges;
    if (remember) {
      //this.storage.set(environment.authata_key, this.authData);
      // this.userIdle.startWatching();
    }

  }

  
  isAuthenticated(): boolean {
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    return this.user && this.user.id > 0

  }

  hasPrivilege(state): boolean {
    console.log('state has privilege', state);
    // if (route.data && route.data['preload'])
    let result = false;

    // this.authData.privileges.forEach(element => {
    //   if (element === name) {
    //     result = true;
    //   }
    // });
    return true;
    // return result;
  }
}
