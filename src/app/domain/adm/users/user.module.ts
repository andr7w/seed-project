import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AccountService } from './services/account.service';
import { IdleService } from './services/idle.service';




@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports :[
  ],
  providers:[
    {provide : AccountService}
  ]


})
export class UserModule { }
