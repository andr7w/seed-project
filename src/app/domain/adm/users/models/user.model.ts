export class UserModel {
    id?: number;
    login: string;
    passwd?: string;
    name: string;
    phone: string;
    email: string;
    code: string;
    changepwd: string;
    active: boolean;
    token?: string;
    isAuth: boolean;
    rights: Map<string, boolean>;
  }
  
  