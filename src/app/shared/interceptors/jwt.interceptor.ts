import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor,  HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor() { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
         
        console.log("Intercepted");
        //console.log(request);
        // let currentUser = this.authService.currentUserValue;
        //add authorization header with jwt token if available
       request = request.clone({headers: request.headers.set('Content-Type', 'application/json')});
       request = request.clone({headers: request.headers.set('Access-Control-Allow-Origin', '*')});
       console.log(request);
    return next.handle(request);
    }
}