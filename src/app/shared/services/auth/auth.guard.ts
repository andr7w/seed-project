import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import {AccountService} from '@domain/adm/index.adm';
import {environment} from '@environments/environment';

@Injectable()
export class AuthGuard implements CanActivate {
  public authToken;
  private isAuthenticated = true; // Set this value dynamically
  
  constructor(
    private router: Router,
    private accountService: AccountService) {}
  
  
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    
      console.log("guard test");
      let activate = this.accountService.isAuthenticated();
      console.log(activate);
      if (!activate) {
        this.router.navigate([environment.signinUrl]);
      } else {
        //activate = this.accountService.hasPrivilege(state);
      }
  
      return true;
    }
}