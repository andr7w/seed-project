import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Subscription } from 'rxjs';

import { Alert, AlertType } from './app-alert.model';
import { AppAlertService } from './app-alert.service';
import { takeUntil, share } from 'rxjs/operators';

@Component({ selector: 'app-alert', templateUrl: 'app-alert.component.html' })
export class AppAlertComponent implements OnInit, OnDestroy {
    @Input() id: string;

    alerts: Alert[] = [new Alert({ message:"message", type: AlertType.Success, alertId: "alert1" })];
    subscription: Subscription;
    test = "test";

    constructor(private alertService: AppAlertService) { }

    ngOnInit() {
        this.subscription = this.alertService.onAlert(this.id)
            .subscribe(alert => {
                console.log("registering..." + this.id);
                if (!alert.message) {
                    // clear alerts when an empty alert is received
                    this.alerts = [];
                    return;
                }
                console.log("pushing...");
                // add alert to array
                this.alerts.push(alert);
            });
    }

    ngOnDestroy() {
        // unsubscribe to avoid memory leaks
        this.subscription.unsubscribe();
    }

    removeAlert(alert: Alert) {
        // remove specified alert from array
        this.alerts = this.alerts.filter(x => x !== alert);
    }

    cssClass(alert: Alert) {
        console.log("calculating css");
        if (!alert) {
            return;
        }

        // return css class based on alert type
        switch (alert.type) {
            case AlertType.Success:
                return 'alert alert-success';
            case AlertType.Error:
                return 'alert alert-danger';
            case AlertType.Info:
                return 'alert alert-info';
            case AlertType.Warning:
                return 'alert alert-warning';
        }
    }
}